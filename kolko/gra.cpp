
/**
 * \file gra.cpp
 * \brief Plik implementacji modułu \a gra.
 *
 * Moduł \a interfejs zawiera funkcje niezbędne
 * do działania programu
 *
 * \see gra.h
 */
#include <iostream>
#include<cstdlib>
#include<fstream>
#include<conio.h>
#include<ctime>
#include"gra.h"
using namespace std;
/// wygrywające kombinacje
const int LINIE[][3][2] =
{
                            { { 0, 0 }, { 0, 1 }, { 0, 2 } },   // górna -
                            { { 1, 0 }, { 1, 1 }, { 1, 2 } },   // środkowa -
                            { { 2, 0 }, { 2, 1 }, { 2, 2 } },   // dolna _
                            { { 0, 0 }, { 1, 0 }, { 2, 0 } },   // lewa |
                            { { 0, 1 }, { 1, 1 }, { 2, 1 } },   // środkowa |
                            { { 0, 2 }, { 1, 2 }, { 2, 2 } },   // prawa |
                            { { 0, 0 }, { 1, 1 }, { 2, 2 } },   // przekątna backslash
                            { { 2, 0 }, { 1, 1 }, { 0, 2 } }    // przekątna slash
};
/** \brief Struktura graczy
 *
 * Definicja Gracza, służąca do prowadzenia statystyk.
 */
struct Sosoba
{
    string imie;
    int wygrane, remisy, przegrane;
};
struct Sosoba komp;
struct Sosoba gracz;
/** \brief  Warunki początkowe
 *
 *Wypełnianie planszy pustymi polami i ustawienie w stan początkowy.
 */
pole g_Plansza[3][3]
{
    {p_puste, p_puste,p_puste,},
    {p_puste, p_puste,p_puste,},
    {p_puste, p_puste,p_puste,}
};
stan g_StanGry = s_poczatek;
znak g_AktualnyGracz,g_Komputer,g_Gracz;
/**
 * @brief Funkcja losująca przy grze z komputerem.
 *
 * Losuje znaki graczy, rozpoczynającego grę i ustawia stan gry
 *
 * @return tak jak reszta funkcji jesli wszystko ok, zwraca true
 */
bool startKomputer()
{
    srand(time(0));
    if (g_StanGry != s_poczatek)
        return false;
    g_Gracz = (rand()% 2 == 0 ? z_kolko : z_krzyzyk);
    g_Komputer = (g_Gracz==z_kolko ? z_krzyzyk : z_kolko);
    g_AktualnyGracz=(rand()% 2 == 0 ? g_Gracz : g_Komputer);
    g_StanGry = s_ruch;
    return true;
}
/**
 * @brief Funkcja wykonująca ruchy przy grze z komputerem.
 *
 * Pobiera pole do ruchu od gracza, wykonuje ruchy komputera,
 * sprawdza stan gry korzystając z funkcji Sprawdz()
 * i odpowiednio go zmienia
 *
 * @return tak jak reszta funkcji jesli wszystko ok, zwraca true
 */
bool ruchKomputer()
{
     srand(time(0));
        int licznik=0,a,b;
        for(;;)
        {       licznik++;
                if(g_AktualnyGracz==g_Gracz) //gracz zaczyna
                    {
                        Rysuj(1);
                        if(g_StanGry == s_ruch)
                                {
                                    int NumerPola;
                                    cin>> NumerPola;
                                    Ruch(NumerPola,'b');
                                    Rysuj(0);
                                    if(g_StanGry == s_wygrana || g_StanGry== s_remis)
                                      {
                                         break;
                                      }
                                }
                    }
                if(g_AktualnyGracz==g_Komputer)
                    {
                        if (licznik==1||licznik==2)
                            {
                               Rysuj(0);
                                if (g_Plansza[1][1]==p_puste)
                                   {
                                        g_Plansza[1][1] = pole(g_Komputer);
                                        g_AktualnyGracz=g_Gracz;
                                   }
                               else
                                 {
                                    do
                                    {
                                        a=rand()%3;
                                        b=rand()%3;
                                    }
                                        while(g_Plansza[a][b]!=p_puste);\
                                    g_Plansza[a][b]=pole(g_Komputer);
                                    g_AktualnyGracz=g_Gracz;
                                 }
                            }
                        if(licznik>2)
                        {
                            {
                               Rysuj(0);
                                if (g_Plansza[1][1]==p_puste)
                                   {
                                        g_Plansza[1][1] = pole(g_Komputer);
                                        g_AktualnyGracz=g_Gracz;
                                   }
                               else
                                 {
                                    do
                                    {
                                        a=rand()%3;
                                        b=rand()%3;
                                    }
                                        while(g_Plansza[a][b]!=p_puste);\
                                    g_Plansza[a][b]=pole(g_Komputer);
                                 }
                            }
                           Sprawdz();
                           if(g_StanGry == s_wygrana || g_StanGry== s_remis)
                             {  Rysuj(0);
                                break;
                             }
                           g_AktualnyGracz=g_Gracz;
                        }
                    }
        }
        return true;
 }
/**
 * @brief Funkcja zerująca statyski
 * Tworzy graczy, zeruje statyski
 */
void osoby()
 {
 komp.imie="komputer";
 gracz.imie="gracz";
 komp.wygrane=0;
 komp.remisy=0;
 komp.przegrane=0;
 gracz.wygrane=0;
 gracz.remisy=0;
 gracz.przegrane=0;
 }
/**
 * @brief Funkcja czyszcząca planszę.
 *
 * Czyści planszę i ustawia grę w stan początkowy
 */
void kasuj()
{
    for (int i=0;i<3;i++)
        for(int j=0;j<3;j++)
        {
            g_Plansza[i][j]=p_puste;
        }
    g_StanGry = s_poczatek;
}
/**
 * @brief Funkcja rozpoczynająca przy grze dwóch graczy
 *
 * Losuje znaki graczy i ustawia stan gry.
 *
 * @return tak jak reszta funkcji jesli wszystko ok, zwraca true
 */
bool Start()
{
    if (g_StanGry != s_poczatek)
        return false;
    srand(time(0));
    g_AktualnyGracz = (rand()% 2 == 0 ? z_kolko : z_krzyzyk); // w ? p:f
    g_StanGry = s_ruch;
    return true;
}
/**
 * @brief Funkcja wykonująca ruchy przy grze dwóch graczy
 *
 * Konwertuje liczby na współrzędne X i Y.
 * Sprawdza czy wybrane pole jest wolne.
 * sprawdza stan gry i zmienia aktualnego gracza.
 *
 * @return tak jak reszta funkcji jesli wszystko ok, zwraca true
 */
bool Ruch(int NumerPola, char wybor)
{
    if (g_StanGry != s_ruch)
        return false;
    if (NumerPola<1 || NumerPola >9)
        return false;
    int Y=(NumerPola-1)/3;
    int X=(NumerPola-1)%3;
    if (g_Plansza[Y][X]==p_puste)
        g_Plansza[Y][X] = pole(g_AktualnyGracz);
        else
        return false;
    pole Pole, ZgodnePole;
    int LiczbaZgodnychPol;
    for (int i=0;i<8;i++)
{
    Pole=ZgodnePole=p_puste;
    LiczbaZgodnychPol=0;
    for(int j=0;j<3;j++)
    {
        Pole=g_Plansza[LINIE[i][j][0]][LINIE[i][j][1]];
        if (Pole!=ZgodnePole)
        {
            ZgodnePole=Pole;
            LiczbaZgodnychPol=1;
        }
            else
            LiczbaZgodnychPol++;
    }
    if (LiczbaZgodnychPol==3 && ZgodnePole!= p_puste)
    {
        g_StanGry=s_wygrana;
        return true;
    }
}
    int LiczbaPelnychPol=0;
    for (int i=0;i<3;i++)
        for(int j=0;j<3;j++)
        {
            if (g_Plansza[i][j]!=p_puste)
                LiczbaPelnychPol++;
        }
    if (LiczbaPelnychPol==9)
    {
         g_StanGry=s_remis;
        return true;
    }
    if (wybor=='a')
    g_AktualnyGracz=(g_AktualnyGracz==z_kolko?z_krzyzyk:z_kolko);
    if (wybor=='b')
    {
            g_AktualnyGracz=g_Komputer;
    }
    return true;
}
/**
 * @brief Funkcja sprawdzająca
 *
 * Sprawdza warunki końcowe, zmienia stan gry.
 *
 * @return tak jak reszta funkcji jesli wszystko ok, zwraca true
 */
bool Sprawdz()
{
    const int LINIE[][3][2] =
{
                                { { 0, 0 }, { 0, 1 }, { 0, 2 } },       // górna -
                                { { 1, 0 }, { 1, 1 }, { 1, 2 } },       // środkowa -
                                { { 2, 0 }, { 2, 1 }, { 2, 2 } },       // dolna _
                                { { 0, 0 }, { 1, 0 }, { 2, 0 } },       // lewa |
                                { { 0, 1 }, { 1, 1 }, { 2, 1 } },       // środkowa |
                                { { 0, 2 }, { 1, 2 }, { 2, 2 } },       // prawa |
                                { { 0, 0 }, { 1, 1 }, { 2, 2 } },       // przekątna backslash
                                { { 2, 0 }, { 1, 1 }, { 0, 2 } }        // przekątna slash
};
    pole Pole, ZgodnePole;
    int LiczbaZgodnychPol;
    for (int i=0;i<8;i++)
{
    Pole=ZgodnePole=p_puste;
    LiczbaZgodnychPol=0;
    for(int j=0;j<3;j++)
    {
        Pole=g_Plansza[LINIE[i][j][0]][LINIE[i][j][1]];
        if (Pole!=ZgodnePole)
        {
            ZgodnePole=Pole;
            LiczbaZgodnychPol=1;
        }
            else
            LiczbaZgodnychPol++;
    }
    if (LiczbaZgodnychPol==3 && ZgodnePole!= p_puste)
    {
        g_StanGry=s_wygrana;
        return true;
    }
}
    int LiczbaPelnychPol=0;
    for (int i=0;i<3;i++)
        for(int j=0;j<3;j++)
        {
            if (g_Plansza[i][j]!=p_puste)
                LiczbaPelnychPol++;
        }
    if (LiczbaPelnychPol==9)
    {
         g_StanGry=s_remis;
        return true;
    }
    return true;
}
/**
 * @brief Funkcja rysująca planszę i zapisująca statystki
 *
 * Rysuje planszę zależnie od wybranego trybu gry.
 * Przy grze z komputerem tworzy statystyki zapisując je do pliku.
 *
 * @return tak jak reszta funkcji jesli wszystko ok, zwraca true
 */
bool Rysuj(int wybor)
 {
     if(g_StanGry==s_poczatek)
         return false;
     system("cls");
         cout << "   KOLKO I KRZYZYK   "<<endl;
         cout << "---------------------" <<endl;
         cout <<endl;
         cout << "        -----" << endl;
         for(int i=0;i<3;i++)
         {
             cout << "        |";
             for(int j=0;j<3;j++)
             {
                 if (g_Plansza[i][j]==p_puste)
                    cout << i * 3 + j + 1;
                 else
                    cout << char(g_Plansza[i][j]);
             }
             cout << "|" << endl;
         }
         cout << "        -----" <<endl;
         cout <<endl;
         switch (g_StanGry)
         {
                 case s_ruch:
             if(wybor==1)
             {
                     cout << "Podaj numer pola, w ktorym" <<endl;
                     cout << "chcesz postawic ";
                     cout << (g_AktualnyGracz == z_kolko ? "kolko" : "krzyzyk") << ": "<<endl;
                     break;
              }
             else
                     break;
                 case s_wygrana:
       if(wybor!=1)
          {
             if(g_AktualnyGracz==g_Gracz)
                {
                 gracz.wygrane++;
                 komp.przegrane++;
                 ofstream plik2("wyniki.txt");
                 plik2<<"\t\tw:\tr:\tp:\n";
                 plik2<<komp.imie<<"\t"<<komp.wygrane<<"\t"<<komp.remisy<<"\t"<<komp.przegrane<<"\n";
                 plik2<<gracz.imie<<"\t\t"<<gracz.wygrane<<"\t"<<gracz.remisy<<"\t"<<gracz.przegrane;
                 plik2.close();
                }
             if(g_AktualnyGracz==g_Komputer)
                {
                 komp.wygrane++;
                 gracz.przegrane++;
                 ofstream plik2("wyniki.txt");
                 plik2<<"\t\tw:\tr:\tp:\n";
                 plik2<<komp.imie<<"\t"<<komp.wygrane<<"\t"<<komp.remisy<<"\t"<<komp.przegrane<<"\n";
                 plik2<<gracz.imie<<"\t\t"<<gracz.wygrane<<"\t"<<gracz.remisy<<"\t"<<gracz.przegrane;
                 plik2.close();
                }
         }
                     cout << "Wygral gracz stawiajacy ";
                     cout << (g_AktualnyGracz == z_kolko ? "kolko" : "krzyzyk") << "!"<<endl;
                     cout <<endl;
                     break;
                 case s_remis:
             if(wybor!=1)
              {
                 gracz.remisy++;
                 komp.remisy++;
                 ofstream plik2("wyniki.txt");
                 plik2<<"\t\tw:\tr:\tp:\n";
                 plik2<<komp.imie<<"\t"<<komp.wygrane<<"\t"<<komp.remisy<<"\t"<<komp.przegrane<<"\n";
                 plik2<<gracz.imie<<"\t\t"<<gracz.wygrane<<"\t"<<gracz.remisy<<"\t"<<gracz.przegrane;
                 plik2.close();
              }
                      cout<< "Remis!"<<endl;
                      cout <<endl;
                     break;
     }
             return true;
 }
