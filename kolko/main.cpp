/**
 * \mainpage
 * \par Kolko i Krzyzyk
 * Gra strategiczna dla dwóch lub jednego gracza.
 * \author Rafał Pokora
 * \date 2013.05.09
 * \version 1.2
 * \par Kontakt:
 * \a rafal.adam.pokora@gmail.com
*/
#include <iostream>
#include<cstdlib>
#include<fstream>
#include<conio.h>
#include"gra.h"
using namespace std;
int main()
{   osoby();
    char wybor;
do
{   system("cls");
    cout<<"Witaj w grze kolko i krzyzyk, wybierz opcje "<<endl;
    cout<<"a)Gra z innym graczem"<<endl;
    cout<<"b)Gra z komputerem"<<endl;
    cout<<"c)Statystyka rozgrywek z komputerem"<<endl;
    cout<<"d)Opis gry"<<endl;
    cout<<"Nacisnij ESC, zeby zakonczyc"<<endl;
    wybor=getch();
        switch(wybor)
{
    case 'a':
            Start();
            for(;;)
                {
                    Rysuj(1);
                        if(g_StanGry == s_ruch)
                            {
                                int NumerPola;
                                cin>> NumerPola;
                                Ruch(NumerPola,wybor);
                            }
                        else
                            if(g_StanGry == s_wygrana || g_StanGry== s_remis)
                            break;
                            cout<<endl;
                }
            kasuj();
            cout<<"Nacisnij dowolny klawisz aby wrocic do menu"<<endl;
            getch();
    break;
    case 'b':
            startKomputer();
            ruchKomputer();
            kasuj();
            cout<<"Nacisnij dowolny klawisz aby wrocic do menu"<<endl;
            getch();
            break;
    case 'c':
          {
            system("cls");
            ifstream plik1;
            plik1.open( "wyniki.txt");
                if( !plik1.good() )
                    {
                        cout<<"blad";
                        return false;
                    }
            string wiersz1;
                while(!plik1.eof())
                    {
                        getline( plik1, wiersz1 );
                        cout << wiersz1 << endl;
                    }
            plik1.close();
            cout<<"Nacisnij dowolny klawisz aby wrocic do menu"<<endl;
            getch();
            break;
        }
    case 'd':
            system("cls");
            ifstream plik;
            plik.open("instrukcja.txt");
                if( !plik.good() )
                    {
                        cout<<"blad";
                        return false;
                    }
            string wiersz;
                while(!plik.eof())
                    {
                        getline( plik, wiersz );
                        cout << wiersz << endl;
                    }
                cout<<"Nacisnij dowolny klawisz aby wrocic do menu"<<endl;
                getch();
            plik.close();
            break;
}
}
    while(wybor!=27);
    return 0;
}
